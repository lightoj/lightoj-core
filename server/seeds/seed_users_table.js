const usersData = [{
  userId: 1,
  userNameStr: 'Test User',
  userHandleStr: 'testuser',
  userEmailStr: 'test@test.com',
  // password is test123
  userPassStr: '$2b$10$vy3Nk6eO8X0k9CJIgvhgxul4XDymKz74htnyr5.POiGI1zL4qnnAi',
  created_at: new Date(),
  updated_at: new Date()
}]

async function cleanUpTables (knex) {
  await knex('problemPermissions').del()
  await knex('problemDescriptionHistory').del()
  await knex('problems').del()
  await knex('social_auth').del()
  await knex('users').del()
}

exports.seed = async function (knex, Promise) {
  // Deletes ALL existing entries
  await cleanUpTables(knex)
  return knex('users').insert(usersData)
}

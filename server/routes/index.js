import { Router } from 'express'
import auth from './auth'
import socialAuth from './socialAuth'
import author from './author'

const router = Router()

// Add USERS Routes
router.use('/auth', auth)
router.use('/social-auth', socialAuth)
router.use('/author', author)

function errorHandler (err, req, res, next) {
  res.status(500)
  res.json({ error: err })
}

router.use(errorHandler)

export default router

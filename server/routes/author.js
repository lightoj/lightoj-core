import {Router} from 'express'
import {check, validationResult} from 'express-validator/check'

import * as authorService from '../services/authorService'
import * as problemService from '../services/problemService'

import {successResponse, errorResponse} from '../utils/response'

const router = Router()

const checkPermissionMiddleware = async (req, res) => {
  const role = await authorService.getUserProblemRole(req.user.id, req.params.handle)
  if (!role || (role !== 'owner' && role !== 'editor')) {
    errorResponse(res, 'You are forbidden', 403)
    throw Error('User does not have enough permission')
  }
  return role
}

const checkProblemExists = async (req, res) => {
  const problem = await authorService.getProblemData(req.params.handle)
  if (!problem) {
    errorResponse(res, 'Problem not found', 404)
    throw Error('Problem not found')
  }
  return problem
}

router.get('/problem/:handle', [],
  async (req, res) => {
    const problem = await checkProblemExists(req, res)
    const role = await checkPermissionMiddleware(req, res)

    const data = {
      role,
      problem
    }
    return successResponse(res, 'OK', data)
  }
)

router.post('/problem/:handle', [
  check('problemData').exists().withMessage('Problem description data is required')
],
async (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return errorResponse(res, errors.array().map(error => error.msg))
  }

  const problem = await checkProblemExists(req, res)
  const role = await checkPermissionMiddleware(req, res)

  const data = {
    role,
    problem
  }
  return successResponse(res, [], data)
})

router.get('/problem/:handle/dataset', [],
  async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array().map(error => error.msg))
    }

    await checkProblemExists(req, res)
    const role = await checkPermissionMiddleware(req, res)

    const datasets = await authorService.getProblemDatasets(req.params.handle)
    const data = {
      role,
      datasets
    }
    return successResponse(res, [], data)
  })

router.post('/problem/:handle/dataset', [],
  async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array().map(error => error.msg))
    }

    await checkProblemExists(req, res)
    const role = await checkPermissionMiddleware(req, res)

    if (req.files.inputFile) {
      if (req.files.inputFile.truncated) {
        return errorResponse(res, 'Input file is too large')
      }
    } else {
      return errorResponse(res, 'Input file is required')
    }

    if (req.files.outputFile) {
      if (req.files.outputFile.truncated) {
        return errorResponse(res, 'Output file is too large', 413)
      }
    } else {
      return errorResponse(res, 'Output file is required')
    }

    const datasetQ = await authorService.addProblemDataset(req.user.id, req.params.handle, req.files, req.body)
    // const problem = await authorService.updateProblemDescription(req.params.handle, req.body.problemData, req.user.id)
    const data = {
      role,
      datasetQ
    }
    return successResponse(res, [], data)
  })

router.patch('/problem/:handle/dataset', [
  check('datasetData').exists().withMessage('Dataset related data is required')
],
async (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return errorResponse(res, errors.array().map(error => error.msg))
  }

  await checkProblemExists(req, res)
  const role = await checkPermissionMiddleware(req, res)

  const datasetQ = await authorService.updateProblemDataset(req.params.handle, req.body.datasetData)

  if (datasetQ == null) {
    return errorResponse(res, 'Something went wrong!')
  }
  const data = {
    role,
    datasetQ
  }
  return successResponse(res, [], data)
})

router.post(
  '/problem',
  [
    check('problemTitle')
      .exists().withMessage('Title field is required')
      .isLength({min: 1}).withMessage('Minimum 1 character is required for the title field'),
    check('problemHandle')
      .exists().withMessage('Handle field is required')
      .isLength({min: 3}).withMessage('Minimum 3 characters are required for the handle field')
      .custom(async (value, {req, loc, path}) => {
        const exist = await problemService.checkHandleExist(value)
        return (exist === true) ? false : value
      }).withMessage('This problem handle already exists')
  ],
  async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array().map(error => error.msg))
    }
    const problem = await authorService.createProblem(req.body.problemTitle, req.body.problemHandle, req.user.id)
    return successResponse(res, [], problem)
  })

router.get(
  '/problems',
  [],
  async (req, res, next) => {
    const problems = await authorService.getProblems(req.user.id, req.body.page, req.body.limit)
    return successResponse(res, [], problems)
  }
)

export default router

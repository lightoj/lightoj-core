
exports.up = function (knex, Promise) {
  return knex.schema.createTable('users', function (t) {
    t.bigIncrements('userId').unsigned().primary()
    t.string('userNameStr').notNullable()
    t.string('userEmailStr').unique().notNullable()
    t.string('userHandleStr', 30).unique().notNullable()
    t.string('userPassStr').notNullable()
    t.timestamps()
    t.timestamp('deleted_at').nullable()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('users')
}

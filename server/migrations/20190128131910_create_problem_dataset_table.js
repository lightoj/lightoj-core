
exports.up = async function (knex, Promise) {
  await knex.schema.createTable('problemDatasets', function (t) {
    t.bigIncrements('datasetId').unsigned().primary()
    t.bigInteger('problemId').unsigned().notNullable().references('problems.problemId')

    t.enu('datasetTypeStr', ['input', 'output'])
    t.string('datasetGroupStr').notNullable()
    t.text('datasetNoteStr').nullable().defaultTo('')
    t.string('datasetHashStr').nullable()
    t.integer('datasetSizeBytesInt').nullable().defaultTo(0)
    t.bigInteger('datasetContentId').unsigned().nullable()
    t.integer('datasetScoreInt').notNullable().defaultTo(100)

    t.boolean('isActiveBool').notNullable().defaultTo(true)
    t.boolean('isSampleBool').notNullable().defaultTo(false)

    t.bigInteger('userId').unsigned().notNullable().references('users.userId')
    t.timestamp('deleted_at').nullable()
    t.timestamps()
  })
}

exports.down = async function (knex) {
  await knex.schema.dropTableIfExists('problemDatasets')
}

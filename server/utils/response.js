export function errorResponse (res, errMessages = [], statusCode = 422) {
  let resp = {
    success: false,
    errors: []
  }

  if (Array.isArray(errMessages)) { // if array then put it as it is
    resp.errors = errMessages
  } else if (errMessages) {
    resp.errors = [errMessages] // single string, make an array
  }
  return res.status(statusCode).json(resp)
}

export function successResponse (res, messages = null, data = null) {
  let resp = {
    success: true,
    messages: [],
    data: data
  }

  if (Array.isArray(messages)) {
    resp.messages = messages
  } else if (messages) {
    resp.messages = [messages]
  }

  return res.status(200).json(resp)
}

import BaseModel from './base'

const TABLE_NAME = 'problemPermissions'

/**
 * ProblemPermission model.
 */
class ProblemPermission extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'problemPermissionId'
  }
}

export default ProblemPermission

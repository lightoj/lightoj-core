import BaseModel from './base'

const TABLE_NAME = 'problemDatasets'

/**
 * ProblemDataset model.
 */
class ProblemDataset extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'datasetId'
  }
}

export default ProblemDataset

import objection from '../db'
import {DbErrors} from 'objection-db-errors'

class BaseModel extends DbErrors(objection.Model) {
  get softDelete () {
    return true
  }

  $beforeUpdate () {
    this.updated_at = new Date().toISOString().slice(0, 19).replace('T', ' ')
  }

  $beforeInsert () {
    this.created_at = new Date().toISOString().slice(0, 19).replace('T', ' ')
    this.updated_at = this.created_at
  }
}

export default BaseModel

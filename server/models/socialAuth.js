import BaseModel from './base'

const TABLE_NAME = 'social_auth'

/**
 * Store Social Authentication Information Of An User
 */
class SocialAuth extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'socialAuthId'
  }
}

export default SocialAuth

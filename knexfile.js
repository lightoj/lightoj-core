const {db} = require('config')
/**
 * Database configuration.
 */
module.exports = {
  client: 'mysql2',
  debug: db.debug,
  asyncStackTraces: db.asyncStackTraces,
  connection: {
    port: 3306,
    host: db.host,
    user: db.username,
    password: db.password,
    database: db.database,
    charset: 'utf8',
    timezone: 'UTC'
  },
  pool: {
    min: 0,
    max: 10
  },
  migrations: {
    tableName: 'migrations',
    directory: './server/migrations'
  },
  seeds: {
    directory: './server/seeds'
  }
}
